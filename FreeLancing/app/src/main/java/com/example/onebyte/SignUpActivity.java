package com.example.onebyte;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.onebyte.Model.Student;
import com.example.onebyte.Model.Teacher;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity {


    Button signUp;
    RadioGroup radioGroup;
    RadioButton rd1,rd2,rd3;
    FirebaseAuth mAuth;
    DatabaseReference refrence;
    EditText password,confirmPassword,email;
    String type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth=FirebaseAuth.getInstance();
        radioGroup=findViewById(R.id.groupradio);
        password=findViewById(R.id.password);
        confirmPassword=findViewById(R.id.confirmPassword);
        email=findViewById(R.id.email);
        rd1=findViewById(R.id.radia_id1);
        rd2=findViewById(R.id.radia_id2);
        signUp=findViewById(R.id.signup);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                rd1 = (RadioButton) findViewById(selectedId);
                final String user=rd1.getText().toString();

                if(email.getText().toString()!=null)
                {
                    if(password.getText().toString().equals(confirmPassword.getText().toString()))
                    {
                        final String emailStr,passwordStr;
                        emailStr=email.getText().toString();
                        passwordStr=password.getText().toString();

                        refrence=FirebaseDatabase.getInstance().getReference().child(user);
                        mAuth.createUserWithEmailAndPassword(emailStr,passwordStr)
                                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                    @Override
                                    public void onSuccess(AuthResult authResult) {
                                        Toast.makeText(getApplicationContext(),"user created",Toast.LENGTH_LONG).show();
                                       // User u = new User(mAuth.getCurrentUser().getUid() , passwordStr, emailStr);
                                        Student student = new Student(mAuth.getCurrentUser().getUid() , emailStr, passwordStr);
                                        //Teacher teacher = new Teacher(mAuth.getCurrentUser().getUid() , emailStr, passwordStr);
                                       // if(user=="Student")
                                      //{
                                            refrence.child(emailStr).setValue(student)
                                                    .addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            Toast.makeText(SignUpActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    })
                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
                                                            Toast.makeText(SignUpActivity.this, "Data Sucessfully added", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                            startActivity(new Intent(SignUpActivity.this, MainActivity.class));

                                        //}
                                        /*if(user=="Teacher")
                                        {
                                            refrence.child(emailStr).setValue(teacher)
                                                    .addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            Toast.makeText(SignUpActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    })
                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
                                                            Toast.makeText(SignUpActivity.this, "Data Sucessfully added", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                            startActivity(new Intent(SignUpActivity.this, MainActivity.class));


                                        }*/


                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(SignUpActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });


                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Password is not confimed",Toast.LENGTH_LONG).show();
                    }
                }
            }

        });

    }


    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radia_id1:
                if (!checked)
                    radioGroup.clearCheck();
                rd1.setChecked(true);
                // Pirates are the best
                break;
            case R.id.radia_id2:
                if (!checked)
                    radioGroup.clearCheck();
                rd2.setChecked(true);
                // Ninjas rule
                break;
        }
    }
}
