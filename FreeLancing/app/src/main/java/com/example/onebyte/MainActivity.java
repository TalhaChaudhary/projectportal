package com.example.onebyte;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button login,signUp;
    RadioGroup radioGroup;
    RadioButton rd1,rd2,rd3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        login=findViewById(R.id.btn_login);
        radioGroup=findViewById(R.id.groupradio);
        rd1=findViewById(R.id.radia_id1);
        rd2=findViewById(R.id.radia_id2);
        signUp=findViewById(R.id.signUp);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                rd1 = (RadioButton) findViewById(selectedId);
                Toast.makeText(MainActivity.this,rd1.getText(), Toast.LENGTH_SHORT).show();
                Intent i=new Intent(MainActivity.this,SignUpActivity.class);
                startActivity(i);
            }
        });
    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radia_id1:
                if (!checked)
                    radioGroup.clearCheck();
                    rd1.setChecked(true);
                    // Pirates are the best
                    break;
            case R.id.radia_id2:
                if (!checked)
                    radioGroup.clearCheck();
                    rd2.setChecked(true);
                    // Ninjas rule
                    break;
        }
    }
}